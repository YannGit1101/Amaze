﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathAnim : MonoBehaviour {

    /*[SerializeField]
    private GameObject deathAnimUI;*/
    public int deathAnim;

    public float delay = 2.0f;
    private float currentTime;
    private float lenght;

	// Use this for initialization
	void Start ()
    {
        //deathAnimUI.SetActive(false);	
	}
	
	// Update is called once per frame
	/*void Update ()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= lenght + delay)
        {
            //deathAnimUI.SetActive(false);
            UI.self.ShowMapScreen();
        }
	}*/

    public void LaunchAnim()
    {
        UI.self.ShowDeathScreen(deathAnim);
        //deathAnimUI.SetActive(true);
        currentTime = 0.0f;
        //lenght = deathAnimUI.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
        //Debug.Log(UI.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        Invoke("HideAnim", delay);
    }

    public void HideAnim()
    {
        UI.self.deathsAnim[deathAnim].SetActive(false);
        UI.self.ShowMapScreen(false);
    }
}
