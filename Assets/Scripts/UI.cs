﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UI : MonoBehaviour
{
    public static UI self;
    void Awake()
    {
        self = this;
    }

    public GameObject mapScreen, winScreen, deathScreen, titleScreen, leaderboardScreen;
    public float winScreenDuration = 3f, deathScreenDuration = 3f;
    public AudioClip titleLoop, ambiantLoop, winJingle, finishJingle;
    public float loopFadeDelay;
    public AudioSource ambiant, oneShot;

    public GameObject[] deathsAnim;

    public GameObject currentScreen;
    AudioClip currentSoundLoop;

    void Start()
    {
        ShowTitleScreen();
        currentScreen.GetComponentInChildren<Text>().transform.DOScale(1.6f, 0.5f).SetLoops(-1, LoopType.Yoyo);
        if (PlayerPrefs.GetInt("first_player") == 0)
        {
            PlayerPrefs.SetInt("first_player", 1);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && currentScreen != deathScreen && currentScreen != winScreen && currentScreen != mapScreen)
        {
            MapDisplay.self.DesactivateCurrentLevel(true);
            ShowTitleScreen();
        }
    }

    void HideCurrentScreen()
    {
        if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            if (currentScreen == titleScreen)
            {
                currentScreen.GetComponentInChildren<Text>().transform.DOTogglePause();
            }
            else if (currentScreen == deathScreen)
            {
                //FindObjectOfType<MouseMove>().Respawn();
                //ShowMapScreen(false);
            }
            else if (currentScreen == winScreen)
            {
                currentScreen = mapScreen;
                currentScreen.SetActive(true);
                MapDisplay.self.StartNextLevel();
            }
        }
    }

    public void ShowTitleScreen()
    {
        ShowScreen(titleScreen);
        PlayLoop(titleLoop);
        titleScreen.GetComponentInChildren<Text>().transform.DOTogglePause();
    }

    public void ShowMapScreen(bool nextLevel)
    {
        ShowScreen(mapScreen);
        if (nextLevel)
        {
            //ShowScreen(mapScreen);
            MapDisplay.self.StartNextLevel();
        }
        else
            MapDisplay.self.StartCurrentLevel();
    }

    public void ShowDeathScreen(int deathAnim)
    {
        FindObjectOfType<MouseMove>().Respawn();
        deathsAnim[deathAnim].SetActive(true);
        ambiant.DOFade(0f, 0.5f).OnComplete(() => ambiant.Pause()); ;
        //deathsAnim[deathAnim].GetComponentInChildren<Animator>().speed = ;
        ShowScreen(deathScreen);
        HUD.self.StopTimer();
        HUD.self.AddDeath();
        MapDisplay.self.DesactivateCurrentLevel(false);
        //Invoke("HideCurrentScreen", deathScreenDuration);
        deathsAnim[deathAnim].GetComponent<AudioSource>().Play();
        deathsAnim[deathAnim].GetComponentInChildren<Animator>().Play("Anim", -1, 0f);
    }

    public void ShowWinScreen()
    {
        ShowScreen(winScreen);
        //ambiant.DOFade(0f, 0.5f).OnComplete(() => ambiant.Pause());
        oneShot.PlayOneShot(winJingle);
        HUD.self.StopTimer();
        //HUD.self.ResetDeaths();
        HUD.self.ModifyTimer(10);
        HUD.self.UpdateScore(100);
        MapDisplay.self.DesactivateCurrentLevel(false);
        Invoke("HideCurrentScreen", winScreenDuration);
    }

    public void ShowLeaderbordScreen()
    {
        oneShot.PlayOneShot(finishJingle);
        ShowScreen(leaderboardScreen);
        ambiant.DOFade(0f, 0.5f).OnComplete(() => ambiant.Pause());
        HUD.self.StopTimer();
        MapDisplay.self.DesactivateCurrentLevel(true);
        leaderboardScreen.GetComponent<Leaderboard>().DisplayScores();
    }

    public void ShowScreen(GameObject newScreen)
    {
        HideCurrentScreen();
        currentScreen = newScreen;
        currentScreen.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    
    public void PlayLoop(string loopName)
    {
        switch (loopName)
        {
            case "ambiant":
                PlayLoop(ambiantLoop);
                break;
            case "title":
                PlayLoop(titleLoop);
                break;
            default:
                break;
        }
    }

    public void PlayLoop(AudioClip newLoop)
    {
        if (currentSoundLoop != null)
        {
            ambiant.DOFade(0f, loopFadeDelay);
            currentSoundLoop = newLoop;
            Invoke("PlayCurrentLoop", loopFadeDelay);
        }
        else
        {
            currentSoundLoop = newLoop;
            PlayCurrentLoop();
        }
    }

    public void StopLoop()
    {
        if (currentSoundLoop != null)
        {
            ambiant.DOFade(0f, loopFadeDelay);
            currentSoundLoop = null;
        }
    }

    void PlayCurrentLoop()
    {
        ambiant.clip = currentSoundLoop;
        ambiant.Play();
        ambiant.DOFade(1f, loopFadeDelay);
    }
}
