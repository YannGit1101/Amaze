﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Intersection))]
[CanEditMultipleObjects]
public class IntersectionEditor : Editor {

    bool left, right, forward, back;
    Intersection inter;

    private Transform handleTransform;
    private Quaternion handleRotation;

	// Update is called once per frame
	public override void OnInspectorGUI()
    {
        inter = target as Intersection;

        //Buttons
        inter.left = EditorGUILayout.Toggle("Left", inter.left);
        inter.forward = EditorGUILayout.Toggle("forward", inter.forward);
        inter.right = EditorGUILayout.Toggle("right", inter.right);
        inter.back = EditorGUILayout.Toggle("back", inter.back);
        if (left != inter.left)
        {
            left = inter.left;
            SetActive(inter, 1, inter.left);
        }
        else if (right != inter.right)
        {
            right = inter.right;
            SetActive(inter, 2, inter.right);
        }
        else if (forward != inter.forward)
        {
            forward = inter.forward;
            SetActive(inter, 3, inter.forward);
        }
        else if (back != inter.back)
        {
            back = inter.back;
            SetActive(inter, 4, inter.back);
        }
    }

    void SetActive(Intersection inter, int nbr, bool active)
    {
        Transform child = inter.transform.GetChild(nbr);
        child.GetChild(0).gameObject.SetActive(active);
        child.GetChild(1).gameObject.SetActive(active);
        child.GetChild(2).gameObject.SetActive(!active);
        EditorUtility.SetDirty(target);
    }
}
