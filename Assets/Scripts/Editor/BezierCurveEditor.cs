﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BezierCurve))]
public class BezierCurveInspector : Editor
{
    private const int lineSteps = 10;

    private BezierCurve curve;
    private Transform handleTransform;
    private Quaternion handleRotation;

    private void OnSceneGUI()
    {
        curve = target as BezierCurve;
        handleTransform = curve.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ?
            handleTransform.rotation : Quaternion.identity;

        Vector3 p0 = ShowPoint(ref curve.left);
        Vector3 p1 = ShowPoint(ref curve.forward);
        Vector3 p2 = ShowPoint(ref curve.right);
        Vector3 p3 = ShowPoint(ref curve.back);

        Handles.color = Color.gray;
        Handles.DrawLine(p0, p1);
        Handles.DrawLine(p1, p2);

        Handles.color = Color.green;
        Vector3 lineStart = curve.GetPoint(0.0f);
        for (int i= 1; i <= lineSteps; i++)
        {
            //Vector3 lineEnd = curve.GetPoint(i / (float)lineSteps);
            Vector3 lineEnd = curve.GetPoint(0, 1, i / (float)lineSteps);
            Handles.DrawLine(lineStart, lineEnd);
            lineStart = lineEnd;
        }
    }

    private Vector3 ShowPoint(ref Vector3 vector)
    {
        Vector3 point = handleTransform.TransformPoint(vector);
        EditorGUI.BeginChangeCheck();
        point = Handles.DoPositionHandle(point, handleRotation);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(curve, "Move Point");
            EditorUtility.SetDirty(curve);
            vector = handleTransform.InverseTransformPoint(point);
        }
        return point;
    }
}
