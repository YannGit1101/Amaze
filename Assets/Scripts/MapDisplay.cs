﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MapDisplay : MonoBehaviour
{
    public static MapDisplay self;
    void Awake()
    {
        self = this;
    }

    public LevelData[] levels;
    public Text levelNbrText;

    AudioSource audioSource;

    LevelData currentLevel;
    int levelCount = -1;
    int arrowCount;

    bool firstLevelFirstTry = true;
    
    void Start () {
        audioSource = GetComponent<AudioSource>();
    }
    
    public void StartNewLevel(int id)
    {
        if (firstLevelFirstTry)
        {
            UI.self.PlayLoop(UI.self.ambiantLoop);
        }

        /*currentLevel = Instantiate(levels[id]);
        currentLevel.transform.SetParent(transform);
        currentLevel.transform.localPosition = Vector3.zero;*/
        levelCount = id;
        currentLevel = levels[id];

        levelNbrText.text = "Maze " + (levelCount + 1);

        StartCurrentLevel();
    }

    public void StartCurrentLevel()
    {
        currentLevel.gameObject.SetActive(true);
        currentLevel.maze.gameObject.SetActive(true);

        GetComponent<CanvasGroup>().alpha = 1f;
        transform.GetChild(0).localScale = transform.GetChild(1).localScale = transform.GetChild(2).localScale = currentLevel.transform.localScale = Vector3.zero;

        arrowCount = 0;

        transform.GetChild(0).DOScale(1f, 0.5f).OnComplete( ()=> transform.GetChild(1).DOScale(1f, 0.5f).OnComplete( ()=> currentLevel.transform.DOScale(2f, 0.5f) ) );

        foreach (Image arrow in currentLevel.arrows)
        {
            arrow.color = new Color(1, 1, 1, 0);
            arrow.GetComponent<RectTransform>().localScale = Vector3.one * 2;
        }
        InvokeRepeating("DisplayNextArrow", 2f, currentLevel.mapDisplayDuration / currentLevel.arrows.Length);
    }

    void DisplayNextArrow()
    {
        currentLevel.arrows[arrowCount].DOFade(1f, 0.3f);
        currentLevel.arrows[arrowCount].GetComponent<RectTransform>().DOScale(1f, 0.3f);
        arrowCount++;

        Invoke("PlayArrowSound", currentLevel.mapDisplayDuration / (currentLevel.arrows.Length*2));

        if (arrowCount == currentLevel.arrows.Length)
        {
            CancelInvoke("DisplayNextArrow");
            Invoke("StartGame", 1f);
        }
    }

    void PlayArrowSound()
    {
        audioSource.PlayOneShot(currentLevel.sounds[arrowCount-1], 1f);
    }

    public void StartGame()
    {
        transform.GetChild(2).DOScale(1f, 0.5f);
        GetComponent<CanvasGroup>().DOFade(0f, 0.5f).OnComplete( ()=> gameObject.SetActive(false) );
        UI.self.currentScreen = null;

        if (firstLevelFirstTry)
        {
            HUD.self.StartTimeAttack(/*currentLevel.maxTime*/);
            firstLevelFirstTry = false;
        }
        else
            HUD.self.ResumeTimer();

        FindObjectOfType<MouseMove>().Spawn(currentLevel.spawn);
        UI.self.ambiant.Play();
        UI.self.ambiant.DOFade(1f, 0.5f);
        //UI.self.PlayLoop("ambiant");
    }

    public void StartNextLevel()
    {
        //gameObject.SetActive(true);
        levelCount++;
        if (levelCount == levels.Length)
        {
            DesactivateCurrentLevel(true);
            UI.self.ShowLeaderbordScreen();
        }
        else
        {
            StartNewLevel(levelCount);
        }
    }

    public void DesactivateCurrentLevel(bool resetLevelCount)
    {
        currentLevel.gameObject.SetActive(false);
        currentLevel.maze.gameObject.SetActive(false);
        if (resetLevelCount)
        {
            levelCount = -1;
            HUD.self.StopTimer();
            HUD.self.ResetDeaths();
            firstLevelFirstTry = true;
        }
    }
}
