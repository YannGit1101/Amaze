﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCamera : MonoBehaviour {

    public MouseMove mouse;

    private Transform trans;
    private Transform mouseTrans;
    private Vector3 initDif;
    private MoveEnum moveEnum = MoveEnum.SimpleMove;

    private Vector3 startPos;
    private Quaternion startRot;
    private float ratio;

	// Use this for initialization
	void Start ()
    {
        trans = transform;
        mouseTrans = mouse.transform;
        initDif = trans.position - mouseTrans.position;
        ratio = 0.0f;
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        
	    if (moveEnum == MoveEnum.SimpleMove)
        {
            trans.position = (mouseTrans.position + initDif);
            //trans.RotateAround(mouseTrans.position, new Vector3(0.0f, 1.0f, 0.0f), Vector3.Angle(mouseTrans.forward, trans.forward));
            //trans.rotation = mouseTrans.rotation;
        }
        else if (moveEnum == MoveEnum.Spline)
        {
            ratio += Time.deltaTime * 4;
            trans.position = Vector3.Lerp(startPos, mouseTrans.position +
                mouseTrans.rotation * initDif, ratio);
            trans.rotation = Quaternion.Lerp(startRot, mouseTrans.rotation, ratio);
        }
	}

    public void ChangeMove(MoveEnum state)
    {
        moveEnum = state;
        ratio = 0.0f;
        startPos = trans.position;
        startRot = trans.rotation;
    }

    public void Stop()
    {
        moveEnum = MoveEnum.Stop;
    }
}
