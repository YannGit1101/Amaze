﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum MoveEnum { SimpleMove, Spline, Stop}

public class MouseMove : MonoBehaviour {

    public MyCamera mouseCamera;
    public Animator animator;
    public AudioSource mouseAudioSource;
    public float mouseAnimSpeed;

    public float speed = 2.0f;
    private float splineRatio = 0.0f;
    private int start, end;
    
    public MoveEnum moveEnum = MoveEnum.SimpleMove;
    public Transform spawn;

    private Transform trans;
    private Intersection currentInter;
    private BezierCurve currentCurve;

    AudioSource audioSource;
    public AudioClip forwardSound, leftSound, rightSound;

    public GameObject inputCanvas;

    void Start ()
    {
        trans = transform;
        audioSource = GetComponent<AudioSource>();
        //mouseAudioSource.Pause();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (moveEnum == MoveEnum.Stop)
        {
            if (Input.GetAxis("Horizontal") > 0.2f)
            {
                start = FindNearest(currentCurve.points);
                SetEnd(start -1);
                if (CheckDirection(end))
                {
                    ChooseDirection();
                    audioSource.PlayOneShot(rightSound);
                }
            }
            else if (Input.GetAxis("Horizontal") < -0.2f)
            {

                start = FindNearest(currentCurve.points);
                SetEnd(start + 1);
                if (CheckDirection(end))
                {
                    ChooseDirection();
                    audioSource.PlayOneShot(leftSound);
                }
            }
            else if (Input.GetAxis("Vertical") > 0.2f)
            {
                start = FindNearest(currentCurve.points);
                SetEnd(start + 2);
                if (CheckDirection(end))
                {
                    ChooseDirection();
                    audioSource.PlayOneShot(forwardSound);
                }
            }
        }
	    else if (moveEnum == MoveEnum.SimpleMove)
        {
            trans.Translate(trans.forward * speed * Time.deltaTime, Space.World);
        }
        else if (moveEnum == MoveEnum.Spline)
        {
            splineRatio += Time.deltaTime * speed;
            if (splineRatio >= 1)
            {
                splineRatio = 1.0f;
                moveEnum = MoveEnum.SimpleMove;
            }
            trans.forward = currentCurve.GetVelocity(start, end, splineRatio);
            Vector3 newPos = currentCurve.GetPoint(start, end, splineRatio);
            animator.speed = Vector3.Distance(newPos, trans.position) * mouseAnimSpeed;
            trans.position = new Vector3(newPos.x, trans.position.y, newPos.z);         
        }
	}

    bool CheckDirection(int point)
    {
        if (!currentInter.left && point == 0)
            return false;
        if (!currentInter.forward && point == 1)
            return false;
        if (!currentInter.right && point == 2)
            return false;
        if (!currentInter.back && point == 3)
            return false;

        return true;
    }

    void ChooseDirection(float rot)
    {
        trans.Rotate(new Vector3(0.0f, rot, 0.0f), Space.Self);
        moveEnum = MoveEnum.SimpleMove;
    }

    void SetEnd(int _end)
    {
        end = _end;
        if (end > 3)
            end -= 4;
        else if (end < 0)
            end += 4;   
    }

    void ChooseDirection()
    {
        moveEnum = MoveEnum.Spline;
        splineRatio = 0.0f;
        mouseCamera.ChangeMove(MoveEnum.Spline);

        mouseAudioSource.DOFade(1f, 0.5f);
        inputCanvas.SetActive(true);
    }

    int FindNearest(Vector3[] pos)
    {
        Vector3 mousePos = trans.position;
        Vector3 interPos = currentInter.transform.position;
        float minDist = Mathf.Infinity;
        float dist = 0;
        int closest = 0;

        for (int i = 0; i < pos.Length; i++)
        {
            dist = Vector3.Distance(mousePos, pos[i] + interPos);
            if (dist < minDist)
            {
                closest = i;
                minDist = dist;
            }
        }
        return closest;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TriggerStop" && moveEnum == MoveEnum.SimpleMove)
        {
            moveEnum = MoveEnum.Stop;
            
            mouseAudioSource.DOFade(0f, 0.5f);
            currentInter = other.transform.parent.GetComponent<Intersection>();
            currentCurve = currentInter.GetComponent<BezierCurve>();
            animator.speed = 0;
            Debug.Log(currentCurve);
        }
        else if (other.tag == "TriggerBagEnd")
        {
            ChooseDirection(180.0f);
        }
        else if (other.tag == "DeathZone")
        {
            //UI.self.ShowScreen("death");
            if (other.GetComponent<DeathAnim>())
                other.GetComponent<DeathAnim>().LaunchAnim();
            StopMove();
            /*mouseCamera.Stop();*/
        }
        else if (other.tag == "Exit")
        {
            mouseAudioSource.Stop();
            UI.self.ShowWinScreen();
        }
    }

    public void Spawn(Transform spawn)
    {
        this.spawn = spawn;
        trans.position = spawn.position;
        trans.rotation = spawn.rotation;
        moveEnum = MoveEnum.SimpleMove;
        mouseAudioSource.Play();
    }

    public void Respawn()
    {
        if (spawn != null)
        {
            //StopMove();
            trans.position = spawn.position;
            trans.rotation = spawn.rotation;
        }
        else
            Debug.LogError("Spawn not found");
    }

    public void StopMove()
    {
        moveEnum = MoveEnum.Stop;
        animator.speed = 0;
        mouseAudioSource.Stop();
    }
}
