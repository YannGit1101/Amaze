﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Intersection : MonoBehaviour
{
    public bool left;
    public bool right;
    public bool forward;
    public bool back;

    private BezierCurve curve;

    public Vector3 leftPoint = new Vector3(-0.5f, 0.0f, 0.0f);
    public Vector3 forwardPoint = new Vector3(0f, 0.5f, 0.0f);
    public Vector3 rightPoint = new Vector3(0.5f, 0.0f, 0.0f);
    public Vector3 backPoint = new Vector3(0f, -0.5f, 0.0f);

    private Vector3[] vectorTab;
    public Vector3[] VectorTab { get { return vectorTab; } set { } }

    void Start()
    {
        vectorTab = new Vector3[4];
        vectorTab[0] = leftPoint;
        vectorTab[1] = forwardPoint;
        vectorTab[2] = rightPoint;
        vectorTab[3] = backPoint;
    }
}
