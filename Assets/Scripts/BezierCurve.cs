﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurve : MonoBehaviour {

    public Vector3 left = new Vector3(-0.5f, 0.0f, 0.0f);
    public Vector3 forward = new Vector3(0f, 0.0f, 0.5f);
    public Vector3 right = new Vector3(0.5f, 0.0f, 0.0f);
    public Vector3 back = new Vector3(0f, 0.0f, -0.5f);
    private Vector3 center = Vector3.zero;

    [HideInInspector]
    public Vector3[] points;

    public static class Bezier
    {
        public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
        {
            return Vector3.Lerp(Vector3.Lerp(p0, p1, t), Vector3.Lerp(p1, p2, t), t);
        }

        public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, float t)
        {
            return 2f * (1f - t) * (p1 - p0) +
                    2f * t * (p2 - p1);
        }
    }


    public void Reset()
    {
        left = new Vector3(-0.5f, 0.0f, 0.0f);
        forward = new Vector3(0f, 0.0f, 0.5f);
        right = new Vector3(0.5f, 0.0f, 0.0f);
        back = new Vector3(0f, 0.0f, -0.5f);

        points = new Vector3[4];
        points[0] = left;
        points[1] = forward;
        points[2] = right;
        points[3] = back;
    }

    public Vector3 GetVelocity(float t)
    {
        return transform.TransformPoint(Bezier.GetFirstDerivative(
            points[0], points[1], points[2], t)) - transform.position;
    }

    public Vector3 GetVelocity(int start, int end, float t)
    {
        return transform.TransformPoint(Bezier.GetFirstDerivative(
            points[start], center, points[end], t)) - transform.position;
    }

    public Vector3 GetPoint(float t)
    {
        return transform.TransformPoint(Bezier.GetPoint(points[0], points[1], points[2], t));
    }

    public Vector3 GetPoint(int start, int end, float t)
    {
        return transform.TransformPoint(Bezier.GetPoint(points[start], center, points[end], t));
    }

}
