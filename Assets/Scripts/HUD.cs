﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HUD : MonoBehaviour {

    public static HUD self;
    void Awake()
    {
        self = this;
    }

    public int timeAttackInitialTime = 60;
    
    int unitSec, deciSec, minutes;
    public int remainingTime;
    public int deaths;

    public Text chrono, deathCount;

    public int score = 100;

    public void StartChrono () {
        InvokeRepeating("AddOneSec", 1f, 1f);
    }

    public void StartTimer(int initialTime)
    {
        remainingTime = initialTime;
        ChangeTimerText(remainingTime.ToString());
        InvokeRepeating("Decrease1Sec", 1f, 1f);
    }

    public void StartTimeAttack()
    {
        remainingTime = timeAttackInitialTime;
        ChangeTimerText(remainingTime.ToString());
        InvokeRepeating("Decrease1Sec", 1f, 1f);
    }

    void AddOneSec()
    {
        unitSec++;
        if (unitSec == 10) {
            unitSec = 0;
            deciSec++;
        }
        if(deciSec == 60)
        {
            deciSec = 0;
            minutes++;
        }
        ChangeTimerText(minutes + ":" + deciSec + "" + unitSec);
    }

    void Decrease1Sec() {
        ModifyTimer(-1);
    }

    public void ModifyTimer(int delta)
    {
        remainingTime += delta;

        if (remainingTime == 0)
        {
            MapDisplay.self.DesactivateCurrentLevel(true);
            UI.self.ShowLeaderbordScreen();
        }
        else
            ChangeTimerText(remainingTime.ToString());
    }

    public void StopChrono()
    {
        CancelInvoke("AddOneSec");
    }

    public void StopTimer()
    {
        CancelInvoke("Decrease1Sec");
    }

    public void ResumeTimer()
    {
        InvokeRepeating("Decrease1Sec", 1f, 1f);
    }

    public void AddDeath()
    {
        deaths++;
        UpdateScore(-50);
        deathCount.text = deaths.ToString();
    }

    public void ResetDeaths()
    {
        deaths = 0;
        score = 100;
        deathCount.text = deaths.ToString();
    }

    void ChangeTimerText(string value)
    {
        chrono.text = value;
        chrono.transform.DOScale(1.2f, 0.2f).OnComplete( ()=> chrono.transform.DOScale(1f, 0.2f) );
    }

    public void UpdateScore(int delta)
    {
        score += delta;
    }
}
