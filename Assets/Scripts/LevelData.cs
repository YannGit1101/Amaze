﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelData : MonoBehaviour {

    public GameObject maze;
    public Transform spawn;
    public Image[] arrows;
    public AudioClip[] sounds;
    public float mapDisplayDuration = 3f;
    public int maxTime = 7;
}
