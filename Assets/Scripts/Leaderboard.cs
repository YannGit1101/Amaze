﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour {

    public Transform[] leaderboard;

    int[] /*playerTimes, */playerScores;

    int nbrOfPlayers = 4;

    int /*lastTime, */lastScore, lastRank = -1;

	// Use this for initialization
	public void DisplayScores () {
        //playerTimes = new int[nbrOfPlayers];
        playerScores = new int[nbrOfPlayers];

        //lastTime = HUD.self.remainingTime;
        lastScore = HUD.self.score;
        Debug.Log(lastScore);
        HUD.self.ResetDeaths();
        lastRank = -1;

        Debug.Log(PlayerPrefs.GetInt("first_player"));
        if(PlayerPrefs.GetInt("first_player") == 1)
        {
            SetPlayerRank(0);
            PlayerPrefs.SetInt("first_player", 0);
        }

        for (int i=0; i < nbrOfPlayers; i++)
        {
            if (playerScores[i] > 0 || PlayerPrefs.GetInt("player_" + i + "_deaths") > 0)
            {
                if (PlayerPrefs.GetInt("player_" + i + "_deaths") > 0)
                    playerScores[i] = PlayerPrefs.GetInt("player_" + i + "_deaths");

                //Debug.Log(lastRank + " " + lastScore + " " + playerScores[i]);
                if(lastRank == -1 && lastScore >= playerScores[i])
                {
                    SetPlayerRank(i);
                }
            }
            else if (lastRank == -1)
                SetPlayerRank(i);

            if(playerScores[i] == 0 && lastRank != i)
            {
                Debug.Log(lastRank + " " + i);
                leaderboard[i].gameObject.SetActive(false);
            }
            else
            {
                Debug.Log(lastRank + " " + i + " " + playerScores[i]);
                leaderboard[i].gameObject.SetActive(true);
                Text[] texts = leaderboard[i].GetComponentsInChildren<Text>();

                if(lastRank == i)
                {
                    texts[0].text = "you";
                    texts[0].color = Color.yellow;
                }
                else
                {
                    texts[0].text = (i + 1).ToString();
                    texts[0].color = Color.white;
                }

                //texts[1].text = playerTimes[i].ToString();
                texts[1].text = playerScores[i].ToString();
            }
        }
    }

    void SetPlayerRank(int rank)
    {
        Debug.Log("player rank is " + rank);
        PlayerPrefs.SetInt("player_" + (rank + 1) + "_deaths", playerScores[rank]);
        playerScores[rank + 1] = playerScores[rank];

        lastRank = rank;
        PlayerPrefs.SetInt("player_" + rank + "_deaths", lastScore);
        playerScores[rank] = lastScore;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
